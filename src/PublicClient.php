<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 22.01.18
 * Time: 22:23
 */

namespace Netborg\Bitbay;

use Curl\Curl;
use Netborg\Bitbay\Model\Bag;
use Netborg\Bitbay\Model\Market;
use Netborg\Bitbay\Model\OrderBook;
use Netborg\Bitbay\Model\Ticker;
use Netborg\Bitbay\Model\Tradebook;

class PublicClient
{

    public const API_VERSION = "2.0";

    protected const ENDPOINT = "https://bitbay.net/API/Public/[CURRENCY1][CURRENCY2]/[CATEGORY].json";

    protected static $redis;




    public static function redis($driver=null, $config=[])
    {
        $config = include 'config/config.php';
        if ($driver && isset($config[$driver]['default'])) {
            static::$redis = array_merge($config[$driver]['default'], $config);
        } else {
            static::$redis = array_merge($config['predis']['default'], $config);
        }
    }


    /**
     * @param string $category
     * @param string $currency1
     * @param string $currency2
     * @return bool|Bag|Market|OrderBook|Ticker|Tradebook|null
     * @throws \ErrorException
     */
    public function execute(string $category, string $currency1, string $currency2)
    {
        $curl = new Curl();
        $curl->get(str_replace([
            '[CURRENCY1]',
            '[CURRENCY2]',
            '[CATEGORY]'
        ],[
            $currency1,
            $currency2,
            $category
        ], self::ENDPOINT));

        if ($curl->error) {
            return false;
        }

        $ret = null;
        switch($category) {
            case Category::TRADES :
                $ret = new Tradebook($curl->response); break;
            case Category::ORDERBOOK :
                $ret = new OrderBook($curl->response); break;
            case Category::MARKET :
                $ret = new Market($curl->response); break;
            case Category::TICKER :
                $ret = new Ticker($curl->response); break;
            case Category::ALL :
                $ret = new Bag($curl->response); break;
        }

        return $ret;
    }

}