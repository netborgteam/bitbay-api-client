<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 21.01.18
 * Time: 15:13
 */

namespace Netborg\Bitbay\Contract;


interface OrderBookInterface
{

    /**
     * @return array
     */
    public function bids(): array;

    /**
     * @return array
     */
    public function asks(): array;

}