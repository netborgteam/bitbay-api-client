<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 30.01.18
 * Time: 20:20
 */

namespace Netborg\Bitbay\Contract;


interface HistoryPositionInterface
{

    /**
     * @return string
     */
    public function id(): string;

    /**
     * @return string
     */
    public function amount(): string;

    /**
     * @return string
     */
    public function balanceAfter(): string;

    /**
     * @return string
     */
    public function currency(): string;

    /**
     * @return string
     */
    public function operationType(): string;

    /**
     * @return string
     */
    public function time(): string;

    /**
     * @return string
     */
    public function comment(): string;

}