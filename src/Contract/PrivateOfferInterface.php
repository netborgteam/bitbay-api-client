<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 21.01.18
 * Time: 13:26
 */

namespace Netborg\Bitbay\Contract;


interface PrivateOfferInterface
{

    /**
     * @return float
     */
    public function price(): float;

    /**
     * @return float
     */
    public function quantity(): float;

    /**
     * @return string
     */
    public function currency(): string;

}