<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 21.01.18
 * Time: 15:59
 */

namespace Netborg\Bitbay\Contract;


interface TradebookInterface
{

    /**
     * @return array
     */
    public function transactions(): array;

}