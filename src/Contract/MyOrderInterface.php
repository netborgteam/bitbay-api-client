<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 29.01.18
 * Time: 21:52
 */

namespace Netborg\Bitbay\Contract;


interface MyOrderInterface
{

    /**
     * @return int
     */
    public function id(): int;

    /**
     * @return string
     */
    public function currency(): string;

    /**
     * @return string
     */
    public function date(): string;

    /**
     * @return string
     */
    public function paymentCurrency(): string;

    /**
     * @return string
     */
    public function type(): string;

    /**
     * @return string
     */
    public function status(): string;

    /**
     * @return float
     */
    public function units(): float;

    /**
     * @return float
     */
    public function startUnits(): float;

    /**
     * @return float
     */
    public function currentPrice(): float;

    /**
     * @return float
     */
    public function startPrice(): float;

    /**
     * @return bool
     */
    public function isActive(): bool;

}