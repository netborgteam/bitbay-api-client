<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 21.01.18
 * Time: 14:27
 */

namespace Netborg\Bitbay\Contract;


interface Arrayable
{

    /**
     * @return array
     */
    public function toArray(): array;

}