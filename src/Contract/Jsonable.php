<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 21.01.18
 * Time: 14:25
 */

namespace Netborg\Bitbay\Contract;


interface Jsonable
{

    /**
     * @param int $options
     * @return string
     */
    public function toJson(int $options=0): string;

}