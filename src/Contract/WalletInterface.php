<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 23.01.18
 * Time: 19:37
 */

namespace Netborg\Bitbay\Contract;


interface WalletInterface
{

    /**
     * Find and return currency account for selected currency.
     *
     * @param string $currency
     * @return CurrencyAccountInterface|null
     */
    public function account(string $currency): ?CurrencyAccountInterface;


    /**
     * Return current wallet balance for selected currency.
     *
     * @param string $currency
     * @return float
     */
    public function balance(string $currency): float;

    /**
     * Add provided amount to the wallet balance of selected currency.
     * Return wallet balance after paying in.
     *
     * @param string $currency
     * @param float $amount
     * @return float
     */
    public function payIn(string $currency, float $amount): float;


    /**
     * Subtract provided amount from wallet balance of selected currency.
     * Return wallet balance after withdrawal.
     *
     * @param string $currency
     * @param float $amount
     * @return float
     */
    public function withdraw(string $currency, float $amount): float;

    /**
     * Return balance locked in open transactions for selected currency.
     *
     * @param string $currency
     * @return float
     */
    public function locked(string $currency): float;

    /**
     * Lock provided amount as open transaction balance for selected currency.
     *
     * @param string $currency
     * @param float $amount
     * @return mixed
     */
    public function lock(string $currency, float $amount);


    /**
     * Add provided amount to actual balance locked by open transactions.
     * Return locked balance after operation.
     *
     * @param string $currency
     * @param float $amount
     * @return float
     */
    public function addLock(string $currency, float $amount): float;

    /**
     * Subtract provided amount from locked by transactions balance for selected currency.
     * Return locked balance after operation.
     *
     * @param string $currency
     * @param float $amount
     * @return float
     */
    public function subtractLock(string $currency, float $amount): float;

}