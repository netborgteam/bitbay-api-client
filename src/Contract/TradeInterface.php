<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 25.01.18
 * Time: 21:20
 */

namespace Netborg\Bitbay\Contract;


interface TradeInterface
{
    /**
     * Get type of trade.
     *
     * @return string|null
     */
    public function type(): ?string;

    /**
     * Set type of trade.
     *
     * @param string $type
     * @return TradeInterface
     */
    public function setType(string $type): TradeInterface;

    /**
     * Get main currency.
     *
     * @return string|null
     */
    public function currency(): ?string;

    /**
     * Set main currency.
     *
     * @param string $currency
     * @return TradeInterface
     */
    public function setCurrency(string $currency): TradeInterface;

    /**
     * Get trade amount.
     *
     * @return float
     */
    public function amount(): float;

    /**
     * Set trade amount.
     *
     * @param float $amount
     * @return TradeInterface
     */
    public function setAmount(float $amount): TradeInterface;

    /**
     * Get payment currency for trade.
     *
     * @return null|string
     */
    public function paymentCurrency(): ?string;

    /**
     * Set payment currency for trade.
     *
     * @param string $currency
     * @return TradeInterface
     */
    public function setPaymentCurrency(string $currency): TradeInterface;

    /**
     * Get trade rate.
     *
     * @return float|null
     */
    public function rate(): float;

    /**
     * Set trade rate.
     *
     * @param float $rate
     * @return TradeInterface
     */
    public function setRate(float $rate): TradeInterface;

}