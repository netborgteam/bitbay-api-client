<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 21.01.18
 * Time: 15:52
 */

namespace Netborg\Bitbay\Contract;


interface TickerInterface
{

    /**
     * @return float
     */
    public function max(): float;

    /**
     * @return float
     */
    public function min(): float;

    /**
     * @return float
     */
    public function last(): float;

    /**
     * @return float
     */
    public function bid(): float;

    /**
     * @return float
     */
    public function ask(): float;

    /**
     * @return float
     */
    public function vwap(): float;

    /**
     * @return float
     */
    public function average(): float;

    /**
     * @return float
     */
    public function volume(): float;

}