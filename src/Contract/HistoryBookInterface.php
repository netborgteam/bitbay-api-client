<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 30.01.18
 * Time: 21:24
 */

namespace Netborg\Bitbay\Contract;


interface HistoryBookInterface
{

    /**
     * @return array
     */
    public function positions(): array;

}