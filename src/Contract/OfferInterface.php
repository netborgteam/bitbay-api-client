<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 21.01.18
 * Time: 13:26
 */

namespace Netborg\Bitbay\Contract;


interface OfferInterface
{

    /**
     * @return float
     */
    public function rate(): float;

    /**
     * @return float
     */
    public function amount(): float;

    /**
     * @return string
     */
    public function transaction(): string;

}