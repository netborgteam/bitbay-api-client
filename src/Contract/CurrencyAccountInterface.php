<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 23.01.18
 * Time: 19:54
 */

namespace Netborg\Bitbay\Contract;


interface CurrencyAccountInterface
{

    /**
     * Return account currency.
     *
     * @return string
     */
    public function currency(): string;

    /**
     * Return balance available.
     *
     * @return float
     */
    public function available(): float;

    /**
     * Return account balance.
     *
     * @return float
     */
    public function balance(): float;

    /**
     * Add provided amount to account balance.
     * Return account balance after operation.
     *
     * @param float $amount
     * @return float
     */
    public function payIn(float $amount): float;

    /**
     * Subtract provided amount from account balance.
     * Return account balance after operation.
     *
     * @param float $amount
     * @return float
     */
    public function withdraw(float $amount): float;

    /**
     * Return account's locked by transactions balance.
     *
     * @return float
     */
    public function locked(): float;

    /**
     * Lock provided amount.
     *
     * @param float $amount
     * @return mixed
     */
    public function lock(float $amount);

    /**
     * Add provided amount to locked by transactions balance.
     * Return locked balance after operation.
     *
     * @param float $amount
     * @return float
     */
    public function addLock(float $amount): float;

    /**
     * Subtract provided amount from locked balance.
     * Return locked balance after operation.
     *
     * @param float $amount
     * @return float
     */
    public function subtractLock(float $amount): float;

}