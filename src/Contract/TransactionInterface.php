<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 21.01.18
 * Time: 14:32
 */

namespace Netborg\Bitbay\Contract;


interface TransactionInterface
{

    /**
     * @return int
     */
    public function date(): int;

    /**
     * @return float
     */
    public function price(): float;

    /**
     * @return string
     */
    public function type(): string;

    /**
     * @return float
     */
    public function amount(): float;

    /**
     * @return string
     */
    public function tid(): string;

}