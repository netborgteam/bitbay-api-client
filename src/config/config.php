<?php
return [
    'predis' => [
        'default' => [
            'host' => 'localhost',
            'password' => null,
            'port' => 6379,
            'database' => 0,
        ],
    ],
];