<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 23.01.18
 * Time: 19:33
 */

namespace Netborg\Bitbay;


use Netborg\Bitbay\Contract\Arrayable;
use Netborg\Bitbay\Contract\Jsonable;
use Netborg\Bitbay\Model\ErrorCode;

class PrivateResponse implements Arrayable, Jsonable
{


    protected $response;


    public function __construct(string $json)
    {
        $this->response = json_decode($json, true);
    }


    /**
     * @return mixed|ErrorCode
     */
    public function result()
    {
        if (isset($this->response['code'])) {
            return new ErrorCode($this->response);
        }

        return $this->response;
    }


    /**
     * @return array
     */
    public function toArray(): array
    {
        // TODO: Implement toArray() method.
    }

    /**
     * @param int $options
     * @return string
     */
    public function toJson(int $options = 0): string
    {
        // TODO: Implement toJson() method.
    }
}