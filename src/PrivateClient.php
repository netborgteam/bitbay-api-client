<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 22.01.18
 * Time: 22:52
 */

namespace Netborg\Bitbay;


use Curl\Curl;
use Netborg\Bitbay\Contract\TradeInterface;
use Netborg\Bitbay\Contract\WalletInterface;
use Netborg\Bitbay\Model\ErrorCode;
use Netborg\Bitbay\Model\HistoryBook;
use Netborg\Bitbay\Model\MyOrderBook;
use Netborg\Bitbay\Model\PrivateOrderBook;
use Netborg\Bitbay\Model\Wallet;

class PrivateClient
{


    protected const ENDPOINT = "https://bitbay.net/API/Trading/tradingApi.php";

    protected static $redis;

    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $secret;




    public static function redis($driver=null, $config=[])
    {
        $config = include 'config/config.php';
        if ($driver && isset($config[$driver]['default'])) {
            static::$redis = array_merge($config[$driver]['default'], $config);
        } else {
            static::$redis = array_merge($config['predis']['default'], $config);
        }
    }





    public function __construct(string $apiKey, string $secret)
    {
        $this->key = $apiKey;
        $this->secret = $secret;
    }

    /**
     * @param array $post
     * @return string
     */
    protected function sign(string $post): string
    {
        return hash_hmac("sha512", $post, $this->secret);
    }


    /**
     * @param string $method
     * @param array $params
     * @return PrivateResponse
     * @throws \ErrorException
     */
    public function execute(string $method, array $params=[]): PrivateResponse
    {
        $curl = new Curl();

        $post = $curl->buildPostData(array_merge($params, [
            'method' => $method,
            'moment' => time(),
        ]));

        $curl->setHeader('API-Key', $this->key);
        $curl->setHeader('API-Hash', $this->sign($post));

        $result = $curl->post(self::ENDPOINT, $post);

        return new PrivateResponse($result);
    }

    /**
     * Get User wallet with currency accounts.
     *
     * @param string|null $currency
     * @return mixed|ErrorCode|Wallet|null
     */
    public function getWallet(string $currency=null)
    {
        try {
            $response = $currency
                ? $this->execute('info', ['currency' => $currency])
                : $this->execute('info');
        } catch (\ErrorException $e) {
            return null;
        }

        if (($result = $response->result()) instanceof ErrorCode) {
            return $result;
        }

        return new Wallet(['accounts' => $result['balances']]);
    }


    /**
     * Post your trade to BitBay.net
     *
     * @param $trade
     * @return int|mixed|ErrorCode|null
     * @throws \InvalidArgumentException
     */
    public function postTrade($trade)
    {
        if (is_array($trade)) {
            try {
                $response = $this->execute('trade', $trade);
            } catch (\ErrorException $e) {
                return null;
            }
        } elseif (is_a($trade, TradeInterface::class)) {
            try {
                $response = $this->execute('trade', $trade->toArray());
            } catch (\ErrorException $e) {
                return null;
            }
        } else {
            throw new \InvalidArgumentException("Attribute `trade` must be an array or implements ".TradeInterface::class);
        }

        if (($result = $response->result()) instanceof ErrorCode) {
            return $result;
        }

        if (isset($result['order_id']) && $result['order_id'] !== 0) {
            return (int) $result['order_id'];
        }

        return null;
    }


    /**
     * Cancel your active trade offer.
     *
     * @param int $tradeId
     * @return bool|mixed|ErrorCode|null
     */
    public function cancelTrade(int $tradeId)
    {
        try {
            $response = $this->execute('cancel', [ 'id' => $tradeId ]);
        } catch (\ErrorException $e) {
            return null;
        }

        if (($result = $response->result()) instanceof ErrorCode) {
            return $result;
        }

        if (isset($result['success']) && $result['success'] === 1 && isset($result['order_id']) && $result['order_id'] == $tradeId) {
            return true;
        }

        return false;
    }

    /**
     * Get OrderBook.
     *
     * @param string $orderCurrency
     * @param string $paymentCurrency
     * @return bool|mixed|ErrorCode|PrivateOrderBook|null
     */
    public function orderbook(string $orderCurrency, string $paymentCurrency)
    {
        try {
            $response = $this->execute('orderbook', [
                'order_currency' => $orderCurrency,
                'payment_currency' => $paymentCurrency
            ]);
        } catch (\ErrorException $e) {
            return null;
        }

        if (($result = $response->result()) instanceof ErrorCode) {
            return $result;
        }

        if (isset($result['bids']) && isset($result['asks'])) {
            return new PrivateOrderBook($result);
        }

        return false;
    }

    /**
     * Get active orders.
     *
     * @param int|null $limit
     * @return bool|mixed|ErrorCode|MyOrderBook|null
     */
    public function orders(?int $limit=null)
    {
        try {
            if ($limit) {
                $response = $this->execute('orders', [
                    'limit' => $limit,
                ]);
            } else {
                $response = $this->execute('orders');
            }

        } catch (\ErrorException $e) {
            return null;
        }

        if (($result = $response->result()) instanceof ErrorCode) {
            return $result;
        }

        if (is_array($result) && count($result)) {
            return new MyOrderBook($result);
        }

        return false;
    }


    /**
     * Transfer crypto-currency to the address provided.
     *
     * @param string $currency
     * @param float $amount
     * @param string $address
     * @return bool|mixed|ErrorCode|null
     */
    public function transfer(string $currency, float $amount, string $address)
    {
        try {
            $response = $this->execute('transfer', [
                'currency' => $currency,
                'quantity' => $amount,
                'address' => $address
            ]);
        } catch (\ErrorException $e) {
            return null;
        }

        if (($result = $response->result()) instanceof ErrorCode) {
            return $result;
        }

        if (is_array($result) && $result['success'] == 1) {
            return true;
        }

        return false;
    }

    /**
     * Order withdrawal to a bank account.
     *
     * @param string $currency
     * @param float $amount
     * @param string $account
     * @param string $bic
     * @param bool $express
     * @return bool|mixed|ErrorCode|null
     */
    public function withdraw(string $currency, float $amount, string $account, string $bic, bool $express=false)
    {
        try {
            $response = $this->execute('withdraw', [
                'currency' => $currency,
                'quantity' => $amount,
                'account' => $account,
                'bic' => $bic,
                'express' => $express ? 'true' : 'false',
            ]);
        } catch (\ErrorException $e) {
            return null;
        }

        if (($result = $response->result()) instanceof ErrorCode) {
            return $result;
        }

        if (is_array($result) && $result['success'] == 1) {
            return true;
        }

        return false;
    }


    public function history(string $currency, int $limit=50)
    {
        try {
            $response = $this->execute('history', [
                'currency' => $currency,
                'limit' => $limit,
            ]);
        } catch (\ErrorException $e) {
            return null;
        }

        if (($result = $response->result()) instanceof ErrorCode) {
            return $result;
        }

        if (is_array($result)) {
            return new HistoryBook($result);
        }

        return false;
    }

}