<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 30.01.18
 * Time: 21:35
 */

namespace Netborg\Bitbay\Model;


use Netborg\Bitbay\Contract\HistoryBookInterface;

class HistoryBook extends BaseModel implements HistoryBookInterface
{

    protected static $keys = [
        'positions',
        'operations'
    ];

    public function __construct($arguments=null)
    {
        if (is_string($arguments)) {  // assume it's json string
            $arguments = json_decode($arguments, true);
        }

        if (is_array($arguments)) {
//            $this->arguments['operations'] = [];

            foreach($arguments as $o) {
                $position = new HistoryPosition($o);
                $this->arguments['positions'][] = $position;

                /*if(!in_array($position->operationType(), $this->arguments['operations'])) {
                    $this->arguments['operations'][] = $position->operationType();
                }*/

            }
        }
    }


    /**
     * @return array
     */
    public function positions(): array
    {
        return $this->positions ?: [];
    }
}