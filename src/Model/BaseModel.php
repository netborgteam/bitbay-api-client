<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 21.01.18
 * Time: 14:46
 */

namespace Netborg\Bitbay\Model;


use Netborg\Bitbay\Contract\Arrayable;
use Netborg\Bitbay\Contract\Jsonable;

abstract class BaseModel implements Jsonable, Arrayable
{

    /**
     * @var array
     */
    protected static $keys = [];


    /**
     * @var array
     */
    protected $arguments = [];




    public function __set($name, $value)
    {
        if (in_array($name, static::$keys)) {
            $this->arguments[$name] = $value;
        }
    }

    public function __get($name)
    {
        if(isset($this->arguments[$name])) {
            return $this->arguments[$name];
        }

        return null;
    }




    /**
     * @param int $options
     * @return string
     */
    public function toJson(int $options = 0): string
    {
        return json_encode($this->toArray(), $options);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $out =[];
        foreach($this->arguments as $key => $arg) {
            if (is_object($arg)) {
                $out[$key] = $this->dumpObjectToArray($arg);
                continue;
            } elseif (is_array($arg)) {
                $out[$key] = $this->dumpArray($arg);
            } else {
                $out[$key] = $arg;
            }
        }
        return $out;
    }

    protected function dumpObjectToArray($object)
    {
        $out = [];
        if (is_a($object, BaseModel::class) || is_a($object, Arrayable::class)) {
            $out = $object->toArray();
        } else {
            foreach(get_object_vars($object) as $k => $v) {
                $out[$k] = $v;
            }
        }

        return $out;
    }

    protected function dumpArray(array $array) {
        $out = [];
        foreach($array as $key => $value) {
            if (is_object($value)) {
                $out[$key] = $this->dumpObjectToArray($value);
            } elseif (is_array($value)) {
                $out[$key] = $this->dumpArray($value);
            } else {
                $out[$key] = $value;
            }
        }

        return $out;
    }

}