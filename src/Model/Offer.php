<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 21.01.18
 * Time: 13:25
 */

namespace Netborg\Bitbay\Model;

use Netborg\Bitbay\Contract\OfferInterface;

class Offer extends BaseModel implements OfferInterface
{
    /**
     * @var array
     */
    protected static $keys = [
        'rate',
        'amount',
        'transaction'
    ];


    /**
     * OfferInterface constructor.
     * @param string $transactionType
     * @param array|string $arguments
     */
    public function __construct(string $transactionType, $arguments=[])
    {
        $this->transaction = $transactionType;

        if (is_string($arguments)) {      // assume it's json string
            $arguments = json_decode($arguments, true);
        }

        if (is_array($arguments) && count($arguments) >= 2) {
            $this->rate = floatval($arguments[0]);
            $this->amount = floatval($arguments[1]);
        }

        if(is_object($arguments)) {
            foreach(static::$keys as $key) {
                if (isset($arguments->{$key})) {
                    $this->{$key} = $arguments->{$key};
                }
            }
        }
    }


    /**
     * @return float
     */
    public function rate(): float
    {
        return $this->rate ?: -1;
    }

    /**
     * @return float
     */
    public function amount(): float
    {
        return $this->amount ?: -1;
    }

    /**
     * @return string
     */
    public function transaction(): string
    {
        return $this->transaction ?: '';
    }
}