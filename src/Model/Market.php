<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 21.01.18
 * Time: 18:29
 */

namespace Netborg\Bitbay\Model;


use Netborg\Bitbay\Contract\OrderBookInterface;
use Netborg\Bitbay\Contract\TradebookInterface;

class Market extends BaseModel implements OrderBookInterface, TradebookInterface
{

    protected static $keys = [
        'bids',
        'asks',
        'transactions'
    ];

    /**
     * Market constructor.
     * @param array|string $arguments
     */
    public function __construct($arguments=null)
    {
        if (is_string($arguments)) {  // assume it's json string
            $arguments = json_decode($arguments, true);
        }

        if (is_array($arguments)) {
            foreach($arguments as $key => $value) {
                if ($key == 'bids' && is_array($arguments['bids'])) {
                    foreach($value as $o) {
                        $this->arguments['bids'][] = new Offer('bid', $o);
                    }
                    continue;
                }
                if ($key == 'asks' && is_array($arguments['asks'])) {
                    foreach($value as $o) {
                        $this->arguments['asks'][] = new Offer('ask', $o);
                    }
                    continue;
                }
                if ($key == 'transactions' && is_array($arguments['transactions'])) {
                    foreach($value as $t) {
                        $this->arguments['transactions'][] = new Transaction($t);
                    }
                    continue;
                }
                $this->{$key} = $value;
            }
        }

        if(is_object($arguments)) {
            foreach(static::$keys as $key) {
                if (isset($arguments->{$key})) {
                    if ($key == 'bids' && is_array($arguments->bids)) {
                        foreach($arguments->bids as $o) {
                            $this->arguments['bids'][] = new Offer('bid', $o);
                        }
                        continue;
                    }
                    if ($key == 'asks' && is_array($arguments->asks)) {
                        foreach($arguments->asks as $o) {
                            $this->arguments['asks'][] = new Offer('ask', $o);
                        }
                        continue;
                    }
                    if ($key == 'transactions' && is_array($arguments->transactions)) {
                        foreach($arguments->transactions as $t) {
                            $this->arguments['transactions'][] = new Transaction($t);
                        }
                        continue;
                    }
                    $this->{$key} = $arguments->{$key};
                }
            }
        }
    }


    /**
     * @return array
     */
    public function bids(): array
    {
        return $this->bids ?: [];
    }

    /**
     * @return array
     */
    public function asks(): array
    {
        return $this->asks ?: [];
    }

    /**
     * @return array
     */
    public function transactions(): array
    {
        return $this->transactions ?: [];
    }
}