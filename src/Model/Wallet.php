<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 23.01.18
 * Time: 20:35
 */

namespace Netborg\Bitbay\Model;


use Netborg\Bitbay\Contract\CurrencyAccountInterface;
use Netborg\Bitbay\Contract\WalletInterface;

class Wallet extends BaseModel implements WalletInterface
{

    protected static $keys = [
        'accounts'
    ];


    /**
     * Wallet constructor.
     * @param array|string|null $attributes
     */
    public function __construct($attributes=null)
    {
        if (is_string($attributes)) {   // assume it's json string
            $attributes = json_decode($attributes, true);
        }

        if (!$attributes) {
            $attributes = [
                'accounts' => [],
            ];
        }

        if (is_array($attributes)) {
            foreach($attributes as $key => $value) {
                if ($key == 'accounts') {

                    foreach($value as $currency => $a) {
                        $this->arguments[$key][$currency] = new CurrencyAccount($currency, $a);
                    }
                }
            }
        }
    }




    /**
     * Find and return currency account for selected currency.
     *
     * @param string $currency
     * @return CurrencyAccountInterface|null
     */
    public function account(string $currency): ?CurrencyAccountInterface
    {
        return isset($this->arguments['accounts'][$currency])
            ? $this->arguments['accounts'][$currency]
            : null;
    }


    /**
     * Return current wallet balance for selected currency.
     *
     * @param string $currency
     * @return float
     */
    public function balance(string $currency): float
    {
        return $this->account($currency)
            ? $this->account($currency)->balance()
            : 0;
    }

    /**
     * Add provided amount to the wallet balance of selected currency.
     * Return wallet balance after paying in.
     *
     * @param string $currency
     * @param float $amount
     * @return float
     */
    public function payIn(string $currency, float $amount): float
    {
        return $this->account($currency)
            ? $this->account($currency)->payIn($amount)
            : 0;
    }

    /**
     * Subtract provided amount from wallet balance of selected currency.
     * Return wallet balance after withdrawal.
     *
     * @param string $currency
     * @param float $amount
     * @return float
     */
    public function withdraw(string $currency, float $amount): float
    {
        return $this->account($currency)
            ? $this->account($currency)->withdraw($amount)
            : 0;
    }

    /**
     * Return balance locked in open transactions for selected currency.
     *
     * @param string $currency
     * @return float
     */
    public function locked(string $currency): float
    {
        return $this->account($currency)
            ? $this->account($currency)->locked()
            : 0;
    }

    /**
     * Lock provided amount as open transaction balance for selected currency.
     *
     * @param string $currency
     * @param float $amount
     * @return mixed
     */
    public function lock(string $currency, float $amount)
    {
        if ($account = $this->account($currency)) {
            $account->lock($amount);
        }
    }

    /**
     * Add provided amount to actual balance locked by open transactions.
     * Return locked balance after operation.
     *
     * @param string $currency
     * @param float $amount
     * @return float
     */
    public function addLock(string $currency, float $amount): float
    {
        return $this->account($currency)
            ? $this->account($currency)->addLock($amount)
            : 0;
    }

    /**
     * Subtract provided amount from locked by transactions balance for selected currency.
     * Return locked balance after operation.
     *
     * @param string $currency
     * @param float $amount
     * @return float
     */
    public function subtractLock(string $currency, float $amount): float
    {
        return $this->account($currency)
            ? $this->account($currency)->subtractLock($amount)
            : 0;
    }


}