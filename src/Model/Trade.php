<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 25.01.18
 * Time: 21:26
 */

namespace Netborg\Bitbay\Model;


use Netborg\Bitbay\Contract\TradeInterface;

class Trade extends BaseModel implements TradeInterface
{

    const BUY = "buy";
    const SELL = "sell";


    protected static $keys = [
        'type',
        'currency',
        'amount',
        'payment_currency',
        'rate'
    ];


    /**
     * Trade constructor.
     * @param null $attributes
     */
    public function __construct($attributes=null)
    {
        if(is_string($attributes)) {    // assume it's json string
            $attributes = json_decode($attributes, true);
        }

        if (!$attributes) {
            $attributes = [
                'type' => null,
                'currency' => null,
                'amount' => 0,
                'payment_currency' => null,
                'rate' => 0,
            ];
        }

        if (is_array($attributes)) {
            foreach($attributes as $key => $value) {
                if (in_array($key, ['amount', 'rate'])) {
                    $this->{$key} = floatval($value);
                } else {
                    $this->{$key} = $value;
                }
            }
        }
    }



    /**
     * Get type of trade.
     *
     * @return string|null
     */
    public function type(): ?string
    {
        return $this->type;
    }

    /**
     * Set type of trade.
     *
     * @param string $type
     * @return TradeInterface
     */
    public function setType(string $type): TradeInterface
    {
        if (in_array($type, ['buy', 'sell', 'bid', 'ask'])) {
            $this->type = $type;
        }

        return $this;
    }

    /**
     * Get main currency.
     *
     * @return string|null
     */
    public function currency(): ?string
    {
        return $this->currency;
    }

    /**
     * Set main currency.
     *
     * @param string $currency
     * @return TradeInterface
     */
    public function setCurrency(string $currency): TradeInterface
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * Get trade amount.
     *
     * @return float
     */
    public function amount(): float
    {
        return $this->amount;
    }

    /**
     * Set trade amount.
     *
     * @param float $amount
     * @return TradeInterface
     */
    public function setAmount(float $amount): TradeInterface
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Get payment currency for trade.
     *
     * @return null|string
     */
    public function paymentCurrency(): ?string
    {
        return $this->payment_currency;
    }

    /**
     * Set payment currency for trade.
     *
     * @param string $currency
     * @return TradeInterface
     */
    public function setPaymentCurrency(string $currency): TradeInterface
    {
        if ($this->currency !== $currency) {
            $this->payment_currency = $currency;
        }

        return $this;
    }

    /**
     * Get trade rate.
     *
     * @return float|null
     */
    public function rate(): float
    {
        return $this->rate;
    }

    /**
     * Set trade rate.
     *
     * @param float $rate
     * @return TradeInterface
     */
    public function setRate(float $rate): TradeInterface
    {
        $this->rate = $rate;
        return $this;
    }
}