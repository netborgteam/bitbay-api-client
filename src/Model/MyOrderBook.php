<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 29.01.18
 * Time: 21:48
 */

namespace Netborg\Bitbay\Model;


use Netborg\Bitbay\Contract\OrderBookInterface;

class MyOrderBook extends BaseModel implements OrderBookInterface
{


    public function __construct($arguments=null)
    {
        if (is_string($arguments)) {  // assume it's json string
            $arguments = json_decode($arguments, true);
        }

        if (is_array($arguments)) {
            foreach($arguments as $o) {
                $myOffer = new MyOrder($o);
                if ($myOffer->type() == 'ask') {
                    $this->arguments['asks'][] = $myOffer;
                } elseif ($myOffer->type() == 'bid') {
                    $this->arguments['bids'][] = $myOffer;
                }
            }
        }
    }


    /**
     * @return array
     */
    public function bids(): array
    {
        return $this->bids ?: [];
    }

    /**
     * @return array
     */
    public function asks(): array
    {
        return $this->asks ?: [];
    }
}