<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 21.01.18
 * Time: 14:44
 */

namespace Netborg\Bitbay\Model;

use Netborg\Bitbay\Contract\TransactionInterface;

class Transaction extends BaseModel implements TransactionInterface
{

    /**
     * @var array
     */
    protected static $keys = [
        'date',
        'price',
        'type',
        'amount',
        'tid'
    ];


    /**
     * Transaction constructor.
     * @param array|string $arguments
     */
    public function __construct($arguments=null)
    {
        if (is_string($arguments)) {  // assume it's json string
            $arguments = json_decode($arguments, true);
        }

        if (is_array($arguments)) {
            foreach ($arguments as $key => $value) {
                $this->{$key} = $value;
            }
        }

        if (is_object($arguments)) {
            foreach(static::$keys as $key) {
                if (isset($arguments->{$key})) {
                    $this->{$key} = $arguments->{$key};
                }
            }
        }
    }


    /**
     * @return int
     */
    public function date(): int
    {
        return $this->date ?: -1;
    }

    /**
     * @return float
     */
    public function price(): float
    {
        return $this->price ?: -1;
    }

    /**
     * @return string
     */
    public function type(): string
    {
        return $this->type ?: '';
    }

    /**
     * @return float
     */
    public function amount(): float
    {
        return $this->amount ?: -1;
    }

    /**
     * @return string
     */
    public function tid(): string
    {
        return $this->tid ?: '';
    }
}