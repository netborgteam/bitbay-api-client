<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 23.01.18
 * Time: 21:24
 */

namespace Netborg\Bitbay\Model;


class ErrorCode extends BaseModel
{

    protected static $keys = [
        'code',
        'message',
    ];




    public function __construct($attributes=null)
    {
        if (is_string($attributes)) {   // assume it's json code
            $attributes = json_decode($attributes, true);
        }

        if (is_array($attributes)) {
            foreach($attributes as $key => $value) {
                if (in_array($key, static::$keys)) {
                    $this->{$key} = $value;
                }
            }
        }
    }

    /**
     * Returns error code.
     *
     * @return int|null
     */
    public function code(): ?int
    {
        return $this->code;
    }

    /**
     * Returns error message
     *
     * @return null|string
     */
    public function message(): ?string
    {
        return $this->message;
    }

}