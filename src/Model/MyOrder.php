<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 29.01.18
 * Time: 21:58
 */

namespace Netborg\Bitbay\Model;


use Netborg\Bitbay\Contract\MyOrderInterface;

class MyOrder extends BaseModel implements MyOrderInterface
{

    protected static $keys = [
        'order_id',
        'order_currency',
        'order_date',
        'payment_currency',
        'type',
        'status',
        'units',
        'start_units',
        'current_price',
        'start_price',
    ];


    public function __construct($arguments=[])
    {
        if (is_string($arguments)) {      // assume it's json string
            $arguments = json_decode($arguments, true);
        }

        if (is_array($arguments)) {
            foreach(static::$keys as $key) {
                if (isset($arguments[$key])) {
                    $this->{$key} = $arguments[$key];
                }
            }
        }

        if(is_object($arguments)) {
            foreach(static::$keys as $key) {
                if (isset($arguments->{$key})) {
                    $this->{$key} = $arguments->{$key};
                }
            }
        }
    }




    /**
     * @return int
     */
    public function id(): int
    {
        return $this->order_id;
    }

    /**
     * @return string
     */
    public function currency(): string
    {
        return $this->order_currency;
    }

    /**
     * @return string
     */
    public function date(): string
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function paymentCurrency(): string
    {
        return $this->payment_currency;
    }

    /**
     * @return string
     */
    public function type(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function status(): string
    {
        return $this->status;
    }

    /**
     * @return float
     */
    public function units(): float
    {
        return floatval($this->units);
    }

    /**
     * @return float
     */
    public function startUnits(): float
    {
        return floatval($this->start_units);
    }

    /**
     * @return float
     */
    public function currentPrice(): float
    {
        return floatval($this->current_price);
    }

    /**
     * @return float
     */
    public function startPrice(): float
    {
        return floatval($this->start_price);
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status == 'active';
    }
}