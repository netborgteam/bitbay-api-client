<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 29.01.18
 * Time: 20:33
 */

namespace Netborg\Bitbay\Model;


use Netborg\Bitbay\Contract\OrderBookInterface;
use Netborg\Bitbay\Model\PrivateOffer as Offer;

class PrivateOrderBook extends BaseModel implements OrderBookInterface
{
    protected static $keys = [
        'bids',
        'asks'
    ];


    /**
     * PrivateOrderBook constructor.
     * @param array|string $arguments
     */
    public function __construct($arguments=null)
    {
        if (is_string($arguments)) {  // assume it's json string
            $arguments = json_decode($arguments, true);
        }

        if (is_array($arguments)) {
            foreach($arguments as $key => $value) {
                if ($key == 'bids' && is_array($arguments['bids'])) {
                    foreach($value as $o) {
                        $this->arguments['bids'][] = new Offer($o);
                    }
                    continue;
                }
                if ($key == 'asks' && is_array($arguments['asks'])) {
                    foreach($value as $o) {
                        $this->arguments['asks'][] = new Offer($o);
                    }
                    continue;
                }
                $this->{$key} = $value;
            }
        }

        if(is_object($arguments)) {
            foreach(static::$keys as $key) {
                if (isset($arguments->{$key})) {
                    if ($key == 'bids' && is_array($arguments->bids)) {
                        foreach($arguments->bids as $o) {
                            $this->arguments['bids'][] = new Offer('bid', $o);
                        }
                        continue;
                    }
                    if ($key == 'asks' && is_array($arguments->asks)) {
                        foreach($arguments->asks as $o) {
                            $this->arguments['asks'][] = new Offer('ask', $o);
                        }
                        continue;
                    }
                    $this->{$key} = $arguments->{$key};
                }
            }
        }
    }


    /**
     * @return array
     */
    public function bids(): array
    {
        return $this->bids ?: [];
    }

    /**
     * @return array
     */
    public function asks(): array
    {
        return $this->asks ?: [];
    }
}