<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 21.01.18
 * Time: 18:14
 */

namespace Netborg\Bitbay\Model;


use Netborg\Bitbay\Contract\TradebookInterface;

class Tradebook extends BaseModel implements TradebookInterface
{

    protected static $keys = [
        'transactions'
    ];



    public function __construct($arguments=null)
    {
        if (is_string($arguments)) {  // assume it's json string
            $arguments = json_decode($arguments, true);
        }

        if (is_array($arguments)) {
            foreach($arguments as $t) {
                $this->arguments['transactions'][] = new Transaction($t);
            }
        }
    }


    /**
     * @return array
     */
    public function transactions(): array
    {
        return $this->transactions;
    }
}