<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 29.01.18
 * Time: 20:36
 */

namespace Netborg\Bitbay\Model;


use Netborg\Bitbay\Contract\PrivateOfferInterface;

class PrivateOffer extends BaseModel implements PrivateOfferInterface
{

    /**
     * @var array
     */
    protected static $keys = [
        'price',
        'quantity',
        'currency'
    ];


    /**
     * OfferInterface constructor.
     * @param array|string $arguments
     */
    public function __construct($arguments=[])
    {

        if (is_string($arguments)) {      // assume it's json string
            $arguments = json_decode($arguments, true);
        }

        if (is_array($arguments)) {
            foreach($arguments as $key => $value) {
                if (in_array($key, ['price', 'quantity'])) {
                    $this->{$key} = floatval($value);
                } else {
                    $this->{$key} = $value;
                }
            }
        }

        if(is_object($arguments)) {
            foreach(static::$keys as $key) {
                if (isset($arguments->{$key})) {
                    if (in_array($key, ['price', 'quantity'])) {
                        $this->{$key} = floatval($arguments->{$key});
                    } else {
                        $this->{$key} = $arguments->{$key};
                    }
                }
            }
        }
    }

    /**
     * @return float
     */
    public function price(): float
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function quantity(): float
    {
        return $this->quantity;
    }

    /**
     * @return string
     */
    public function currency(): string
    {
        return $this->currency;
    }
}