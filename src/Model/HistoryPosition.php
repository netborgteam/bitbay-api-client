<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 30.01.18
 * Time: 21:15
 */

namespace Netborg\Bitbay\Model;


use Netborg\Bitbay\Contract\HistoryPositionInterface;

class HistoryPosition extends BaseModel implements HistoryPositionInterface
{

    protected static $keys = [
        'id',
        'amount',
        'balance_after',
        'currency',
        'operation_type',
        'time',
        'comment'
    ];


    public function __construct($arguments=[])
    {
        if (is_string($arguments)) {      // assume it's json string
            $arguments = json_decode($arguments, true);
        }

        if (is_array($arguments)) {
            foreach(static::$keys as $key) {
                if (isset($arguments[$key])) {
                    $this->{$key} = $arguments[$key];
                }
            }
        }

        if(is_object($arguments)) {
            foreach(static::$keys as $key) {
                if (isset($arguments->{$key})) {
                    $this->{$key} = $arguments->{$key};
                }
            }
        }
    }


    /**
     * @return string
     */
    public function id(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function amount(): string
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function balanceAfter(): string
    {
        return $this->balance_after;
    }

    /**
     * @return string
     */
    public function currency(): string
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function operationType(): string
    {
        return $this->operation_type;
    }

    /**
     * @return string
     */
    public function time(): string
    {
        return $this->time;
    }

    /**
     * @return string
     */
    public function comment(): string
    {
        return $this->comment;
    }
}