<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 21.01.18
 * Time: 16:03
 */

namespace Netborg\Bitbay\Model;


use Netborg\Bitbay\Contract\TickerInterface;

class Ticker extends BaseModel implements TickerInterface
{

    protected static $keys = [
        'max',
        'min',
        'last',
        'bid',
        'ask',
        'vwap',
        'average',
        'volume',
    ];


    /**
     * Ticker constructor.
     * @param array|string $arguments
     */
    public function __construct($arguments=null)
    {
        if (is_string($arguments)) {  // assume it's json string
            $arguments = json_decode($arguments, true);
        }

        if (is_array($arguments)) {
            foreach($arguments as $key => $value) {
                $this->{$key} = $value;
            }
        }

        if(is_object($arguments)) {
            foreach(static::$keys as $key) {
                if (isset($arguments->{$key})) {
                    $this->{$key} = $arguments->{$key};
                }
            }
        }
    }


    /**
     * @return float
     */
    public function max(): float
    {
        return $this->max ?: -1;
    }

    /**
     * @return float
     */
    public function min(): float
    {
        return $this->min ?: -1;
    }

    /**
     * @return float
     */
    public function last(): float
    {
        return $this->last ?: -1;
    }

    /**
     * @return float
     */
    public function bid(): float
    {
        return $this->bid ?: -1;
    }

    /**
     * @return float
     */
    public function ask(): float
    {
        return $this->ask ?: -1;
    }

    /**
     * @return float
     */
    public function vwap(): float
    {
        return $this->vwap ?: -1;
    }

    /**
     * @return float
     */
    public function average(): float
    {
        return $this->average ?: -1;
    }

    /**
     * @return float
     */
    public function volume(): float
    {
        return $this->volume ?: -1;
    }
}