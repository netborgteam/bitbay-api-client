<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 23.01.18
 * Time: 20:11
 */

namespace Netborg\Bitbay\Model;


use Netborg\Bitbay\Contract\CurrencyAccountInterface;

class CurrencyAccount extends BaseModel implements CurrencyAccountInterface
{

    protected static $keys = [
        'currency',
        'balance',
        'locked',
        'available',
    ];


    /**
     * CurrencyAccount constructor.
     * @param string $currency
     * @param array|string|null $attributes
     */
    public function __construct(string $currency, $attributes=null)
    {
        if(is_string($attributes)) {    // assume it's json string
            $attributes = json_decode($attributes, true);
        }

        if (!$attributes) {
            $attributes = [
                'balance' => 0,
                'locked' => 0,
                'available' => 0,
            ];
        }

        if (is_array($attributes)) {
            foreach($attributes as $key => $value) {
                if (in_array($key, static::$keys)) {
                    $this->{$key} = floatval($value);
                }
            }
            $this->balance = $this->available + $this->locked;
            $this->currency = $currency;
        }
    }


    /**
     * Return account currency.
     *
     * @return string
     */
    public function currency(): string
    {
        return $this->currency;
    }

    /**
     * Return balance available.
     *
     * @return float
     */
    public function available(): float
    {
        return $this->balance - $this->locked;
    }

    /**
     * Return account balance.
     *
     * @return float
     */
    public function balance(): float
    {
        return $this->balance;
    }

    /**
     * Add provided amount to account balance.
     * Return account balance after operation.
     *
     * @param float $amount
     * @return float
     */
    public function payIn(float $amount): float
    {
        return $this->balance += $amount;
    }

    /**
     * Subtract provided amount from account balance.
     * Return account balance after operation.
     *
     * @param float $amount
     * @return float
     */
    public function withdraw(float $amount): float
    {
        return $this->balance -= $amount;
    }

    /**
     * Return account's locked by transactions balance.
     *
     * @return float
     */
    public function locked(): float
    {
        return $this->locked;
    }

    /**
     * Lock provided amount.
     *
     * @param float $amount
     * @return mixed
     */
    public function lock(float $amount)
    {
        $this->locked = $amount;
    }

    /**
     * Add provided amount to locked by transactions balance.
     * Return locked balance after operation.
     *
     * @param float $amount
     * @return float
     */
    public function addLock(float $amount): float
    {
        return $this->locked += $amount;
    }

    /**
     * Subtract provided amount from locked balance.
     * Return locked balance after operation.
     *
     * @param float $amount
     * @return float
     */
    public function subtractLock(float $amount): float
    {
        return $this->locked -= $amount;
    }


}