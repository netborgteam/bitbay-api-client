<?php
/**
 * Created by PhpStorm.
 * User: netborg
 * Date: 21.01.18
 * Time: 15:43
 */

namespace Netborg\Bitbay;


class Category
{
    public const TRADES = "trades";
    public const ORDERBOOK = "orderbook";
    public const MARKET = "market";
    public const TICKER = "ticker";
    public const ALL = "all";
}