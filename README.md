# BitBay.net - public API client #

It's a free to use PHP [public API v2.0](https://bitbay.net/pl/api-publiczne) and [private API](https://bitbay.net/pl/api-prywatne) client for BitBay.net crypto-currency trading platform.

### Requirements ###

- PHP: 7.1+;
- PHP CURL module;
- Redis (optional);

### Installation ###

```
composer require netborg/bitbay-api-client
```



### Example of usage ###

Example of request to PUBLIC API:
```php
$client = new \Netborg\Bitbay\PublicClient();

$response = $client->execute(\Netborg\Bitbay\Category::ALL, 'BTC', 'PLN');
```

Example of request to PRIVATE API:
```php
$client = new \Netborg\Bitbay\PrivateClient("[API_KEY]", "[SECRET]");

// get User's wallet with all currency accounts
$wallet = $client->getWallet();

// get User's wallet with selected currency
$wallet = $client->getWallet('BTC');

// execute raw request to BitBay.net private API
$response = $client->execute("info", ["currency" => "BTC"]);

```

### Redis Support ###

_This feature is not fully implemented yet._

Optionally you may want to use Redis for fast caching. In such case please add `predis/predis` library:
```
composer require predis/predis
```

Config file you will find in `config` directory. Alternatively you can setup your redis driver:
```php
\Netborg\Bitbay\PublicClient::redis('predis', [
    'host' => 'localhost',
    'password' => null,
    'port' => 6379,
    'database' => 0,
]);

$client = new \Netborg\Bitbay\PublicClient();
```

 

